# FCC-ee optics repository test directory

This directory holds various files, that are executed after every commit and outputs are compared to reference values.
This helps catch any small errors quickly.

The directory is split up as follows:

- in the `template` directory, the templates for different tests are saved.
    They follow the jinja2 template convention.
- the `run_codes.py` script takes the operation mode as argument and fills out the templates with values from the `reference_parameters.json`.
    The filled out scripts are then executed and their output will be saved in the public directory.
    The executable and the templates which are supposed to be used are defined in `run_codes.py`.
- the `test_*.py` read the input and several checks are performed.

Note that for the execution, `run_codes.py` expects a directory called `codes` at the repository top level.
In this directory, a `madx` executable and a `SAD` installation.

In Ubuntu, this can be set up using the following commands:

```bash
apt install -y wget python3 python3-pip gcc gfortran bison git make patch libx11-dev groff-base
mkdir -p codes 
wget -q -O ./codes/madx https://madx.web.cern.ch/madx/releases/last-rel/madx-linux64-gnu && chmod u+x ./codes/madx
cd ./codes 
git clone https://github.com/KatsOide/SAD.git
cd ./SAD 
head -n 70 sad.conf > sad.conf.new && mv -f sad.conf.new sad.conf && make -s exe && make -s install
```

The `test_*.py` files contain a small number of checks to ensure consistency between the `reference_paramters.json` and the lattice.
These checks can be performed using `python -m pytest --operation_mode=OPERATION_MODE`.
Note that in order to merge changes into the main branches, all checks need to pass.

For doing all the above, some python packages may need to be installed.
This can be done by issuing `pip install -r python_requirements.txt`.
