from pathlib import Path
import pytest
import json
import pandas as pd
from pandas._testing import assert_frame_equal, assert_series_equal
import tfs
import numpy as np
import scipy.interpolate as interpld


REPOSITORY_TOP_LEVEL = Path(__file__).resolve().parent.parent
REFERENCE_FILE = json.load(open(REPOSITORY_TOP_LEVEL/"reference_parameters.json"))
TWISS_DIR = REPOSITORY_TOP_LEVEL/'public'

TUNE_ACCURACY = REFERENCE_FILE['TUNE_ACCURACY']
EMITTANCE_ACCURACY = REFERENCE_FILE['EMITTANCE_ACCURACY']
BETA_ACCURACY = REFERENCE_FILE['BETA_ACCURACY']

THIN_REL_TOLERANCE = {'t': 1e-2, 'h':1e-2, 'w':1e-2, 'z':1e-2}
THIN_ABS_TOLERANCE = {'t': 2e-1, 'h': 2e-1,'w': 2e-1,'z':1.2} # refine Z matching at FSCWL to reduce this tolerance

PLANES = ['X', 'Y', 'Z']


def test_emit(operation_mode):
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs", index="NAME")
    assert abs(twiss.headers['EX'] - REFERENCE_FILE[operation_mode]['EMITTANCE_X'])/REFERENCE_FILE[operation_mode]['EMITTANCE_X'] < EMITTANCE_ACCURACY
    assert abs(twiss.headers['EY'] - REFERENCE_FILE[operation_mode]['EMITTANCE_Y'])/REFERENCE_FILE[operation_mode]['EMITTANCE_Y'] < EMITTANCE_ACCURACY
    assert abs(twiss.headers['EY']/twiss.headers['EX'] - REFERENCE_FILE[operation_mode]['EMITTANCE_RATIO']) < EMITTANCE_ACCURACY
    assert abs(twiss.headers['PC'] - REFERENCE_FILE[operation_mode]['ENERGY']) < TUNE_ACCURACY


def test_tune(operation_mode):
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs", index="NAME")
    assert abs(twiss.headers['Q1']%1 - REFERENCE_FILE[operation_mode]['Q1']) < TUNE_ACCURACY
    assert abs(twiss.headers['Q2']%1 - REFERENCE_FILE[operation_mode]['Q2']) < TUNE_ACCURACY


def test_betastar(operation_mode):
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs", index="NAME")
    twiss=twiss[twiss.index.str.contains('^IP\.[1-8]$')] # select all IPs, ensure no other element containing IP fall into that
    for ip in twiss.index:
        assert abs(twiss.loc[ip,'BETX'] - REFERENCE_FILE[operation_mode]['BETASTAR_X']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'BETY'] - REFERENCE_FILE[operation_mode]['BETASTAR_Y']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'ALFX']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'ALFY']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'DX']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'DY']) < BETA_ACCURACY


def test_thin(operation_mode):
    # load thick and thin twiss, take all markers from thick twiss and compare optics functions
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs", index="NAME")
    twiss_thin=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_thin_nottapered.tfs", index="NAME")

    twiss= twiss.loc[twiss['KEYWORD']=='MARKER']
    twiss_thin = twiss_thin.loc[twiss.index]
    # thin twiss has more markers for rematching, use only those present in thick twiss

    for marker in twiss.index:
        assert abs(twiss.loc[marker,'BETX'] - twiss_thin.loc[marker,'BETX'])/twiss.loc[marker,'BETX'] < THIN_REL_TOLERANCE[operation_mode],\
             f"Beta_x at {marker}: Twiss thick: {twiss.loc[marker,'BETX']}, Twiss thin: {twiss_thin.loc[marker,'BETX']}"
        assert abs(twiss.loc[marker,'BETY'] - twiss_thin.loc[marker,'BETY'])/twiss.loc[marker,'BETY'] < THIN_REL_TOLERANCE[operation_mode],\
             f"Beta_y at {marker}: Twiss thick: {twiss.loc[marker,'BETY']}, Twiss thin: {twiss_thin.loc[marker,'BETY']}"

        assert abs(twiss.loc[marker,'ALFX'] - twiss_thin.loc[marker,'ALFX']) < THIN_ABS_TOLERANCE[operation_mode],\
             f"Alpha_x at {marker}: Twiss thick: {twiss.loc[marker,'ALFX']}, Twiss thin: {twiss_thin.loc[marker,'ALFX']}"
        assert abs(twiss.loc[marker,'ALFY'] - twiss_thin.loc[marker,'ALFY']) < THIN_ABS_TOLERANCE[operation_mode],\
             f"Alpha_y at {marker}: Twiss thick: {twiss.loc[marker,'ALFY']}, Twiss thin: {twiss_thin.loc[marker,'ALFY']}"

        assert abs(twiss.loc[marker,'DX'] - twiss_thin.loc[marker,'DX']) < THIN_ABS_TOLERANCE[operation_mode],\
             f"D_x at {marker}: Twiss thick: {twiss.loc[marker,'DX']}, Twiss thin: {twiss_thin.loc[marker,'DX']}"
        assert abs(twiss.loc[marker,'DPX'] - twiss_thin.loc[marker,'DPX']) < THIN_ABS_TOLERANCE[operation_mode],\
             f"D'_x at {marker}: Twiss thick: {twiss.loc[marker,'DPX']}, Twiss thin: {twiss_thin.loc[marker,'DPX']}"


def test_orbitbumps(operation_mode):

    twiss_solenoid=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_solenoid_bump.tfs", index='NAME')
    twiss_corrector=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_corrector_bump.tfs", index='NAME')

    bumps = {
        'IP.1':{'X':0, 'PX':0, 'Y':0, 'PY':0},
        'IP.2':{'X':10e-6, 'PX':0, 'Y':0, 'PY':0},
        'IP.4':{'X':0, 'PX':0, 'Y':10e-6, 'PY':0},
        'IP.6':{'X':0, 'PX':10e-6, 'Y':0, 'PY':0},
    }

    for ip, amplitude in bumps.items():
        for plane, amp in amplitude.items():
            assert abs(twiss_corrector.loc[ip, plane] - amp) < BETA_ACCURACY,\
                f'{ip}:{plane} is {twiss_corrector.loc[ip, plane]}, should be {amp}'


def test_aperture(operation_mode):
    aperture=tfs.read(TWISS_DIR/operation_mode/f"aperture_{operation_mode}_b1_nottapered.tfs", index='NAME')

    assert aperture.headers['N1MIN'] > min(REFERENCE_FILE[operation_mode]['APERTURE']['PRIMARY_COLLIMATOR_APERTURE_X'],
                                           REFERENCE_FILE[operation_mode]['APERTURE']['PRIMARY_COLLIMATOR_APERTURE_Y'])
    # assert 'TCP' in aperture.headers['AT_ELEMENT']
    mask = aperture.index.str.contains('TCP|TCS', regex=True)
    aperture=aperture[~mask]
    assert all(aperture['N1']>REFERENCE_FILE[operation_mode]['APERTURE']['N1'])
    aperture = calc_momentum_acceptance(aperture, REFERENCE_FILE[operation_mode]['APERTURE']['NSIG_FOR_MOMENTUM_ACCEPTANCE'])

    assert all(aperture['MOMENTUM_ACCEPTANCE'][aperture.APER_1>0.0] > REFERENCE_FILE[operation_mode]['APERTURE']['MIN_MOMENTUM_ACCEPTANCE'])


def calc_momentum_acceptance(df, n_sig):

    df['MOMENTUM_ACCEPTANCE'] = (df['APER_1'] - n_sig*np.sqrt(df['BETX']*df.headers['EXN']/(df.headers['BETA']*df.headers['GAMMA'])) )/ abs(df['DX'])

    return df


def test_survey(operation_mode):
    survey=tfs.read(TWISS_DIR/operation_mode/f"survey_madx_{operation_mode}_b1.tfs")

    # check that ring is closed by comparing X,Y,Z of start and endpoint
    assert_series_equal(survey[PLANES].iloc[0],
                       survey[PLANES].iloc[-1],
                       check_exact=False, atol=BETA_ACCURACY, rtol=0.01, check_names=False)

    # begin for more detailed survey comparison between FCC-hh, to be completed and refined
    survey = survey.set_index('NAME')
    xcenter_fccee = (survey.loc['IP.1','X']+survey.loc['IP.2','X'])*0.5
    zcenter_fccee = (survey.loc['IP.1','Z']+survey.loc['IP.2','Z'])*0.5
    Rvatan_fccee = interpld.interp1d(
                                     np.arctan2((survey['Z']-zcenter_fccee),
                                                (survey['X']-xcenter_fccee)),
                                     np.sqrt((survey['X']-xcenter_fccee)**2+(survey['Z']-zcenter_fccee)**2),
                                     fill_value="extrapolate"
                                     )
    theta = np.linspace(-np.pi, np.pi, 1001, endpoint=False)


def test_tuning(operation_mode):
    twiss = tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_tune_matched.tfs", index="NAME")
    
    assert abs(twiss.headers['Q1']%1 - 0.2) < TUNE_ACCURACY
    assert abs(twiss.headers['Q2']%1 - 0.3) < TUNE_ACCURACY
    
    twiss=twiss[twiss.index.str.contains('^IP\.[1-8]$')] # select all IPs, ensure no other element containing IP fall into that
    for ip in twiss.index:
        assert abs(twiss.loc[ip,'BETX'] - REFERENCE_FILE[operation_mode]['BETASTAR_X']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'BETY'] - REFERENCE_FILE[operation_mode]['BETASTAR_Y']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'ALFX']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'ALFY']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'DX']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'DY']) < BETA_ACCURACY