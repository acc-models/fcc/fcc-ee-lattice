!--------------------------------------------------------------
! Example FCC-ee MAD-X script
! This script prepares the {{operation_mode}} lattice.
! The thick sequences is loaded and optics functions are saved to a twiss files.
! Tapering will be applied, and the sequence will be saved to a file.
!--------------------------------------------------------------

SET, FORMAT="19.15f";
option,update_from_parent=true; // new option in mad-x as of 2/2019


!--------------------------------------------------------------
! Lattice selection and beam parameters
!--------------------------------------------------------------

CALL, FILE="../../lattices/{{operation_mode}}/fccee_{{operation_mode}}.seq";

pbeam :=   {{reference['ENERGY']}};
EXbeam = {{reference['EMITTANCE_X']}};
EYbeam = {{reference['EMITTANCE_Y']}};
Nbun :=    {{reference['BUNCHES']}};
NPar :=   {{reference['BUNCH_POPULATION']}};
HalfCrossingAngle = 0.015;


Ebeam := sqrt( pbeam^2 + emass^2 );

// Beam defined without radiation as a start - radiation is turned on later depending on the requirements
BEAM, PARTICLE=ELECTRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;

!--------------------------------------------------------------
! Load aperture definitions and perform sequence edits
!--------------------------------------------------------------

// Load the aperture definition
CALL, FILE="../../aperture/FCCee_aper_definitions.madx";
CALL, FILE="../../aperture/install_synchrotron_rad_masks.madx";
CALL, FILE="../../aperture/install_IP_markers.madx";
CALL, FILE="../../toolkit/macro.madx";


!-------------------------------------------------------------------------------
! Perform initial TWISS and survey in an ideal machine without radiation
!-------------------------------------------------------------------------------

USE, SEQUENCE = FCCEE_P_RING;

// Save the voltage settings for the cavities for later use if needed
VOLTCA1SAVE = VOLTCA1; 
VOLTCA2SAVE = VOLTCA2; 

SHOW, VOLTCA1SAVE, VOLTCA2SAVE;

// Turn off the cavities for ideal machine twiss and survey
VOLTCA1 = 0;
VOLTCA2 = 0;

SAVEBETA, LABEL=B.IP, PLACE=IP.1, SEQUENCE=FCCEE_P_RING;
TWISS;

NEWTUNE_X = FLOOR(TABLE(SUMM, Q1))+0.2;
NEWTUNE_Y = FLOOR(TABLE(SUMM, Q2))+0.3;

exec, CHANGE_TUNE(NEWTUNE_X, NEWTUNE_Y);

TWISS, FILE=twiss_{{operation_mode}}_b1_tune_matched.tfs;