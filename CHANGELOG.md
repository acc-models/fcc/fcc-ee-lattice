## FCC-ee changelog

#### V23.1 
    - New lattice with smaller circumference of 90.6km. RF installed in only one insertion. IP straights now include space for polarimeter. beta^* adjusted to find reasonable lifetime with Beamstrahlung. 
More information and parameter set in the [WP2presentation](https://indico.cern.ch/event/1282220/) and [WP2presentation](https://indico.cern.ch/event/1306350/).
