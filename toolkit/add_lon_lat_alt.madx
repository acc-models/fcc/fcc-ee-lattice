! MADX script to fill in lon, lat and alt values in a tfs survey table
!
! File add_lon_lat_alt.madx was saved on 23/04/2022 17:48 by T.Risselada
!
! Requires existence of survey output file "survey.tfs" (to be read by READTABLE command)
! At least NAME, X, Y and Z should be present (in any order).
! Values of geographic coordinates lon_ref, lat_ref [degrees] and altitude alt_ref [m above sea level]
! of the chosen starting point must be previously defined in the MADX job.
! The conversion to lon-lat-alt uses equations by B.Weyer CERN BE-GM-APC
!
! N.B. the ATAN function works only properly for angles beteen -90 and +90 degrees. No problem for
! accelerators in te Geneva region, but a request has been made to the MADX team to provide an
! ATAN2 function (with two arguments: sine, cosine) to make the script work all around the world.
!
! -------------------------------------------------------------------------------------------------------

 readtable, table=survey, file="survey.tfs";     !! N.B. CPU time depends on number of elements included in the table
 set, format="21.15f";     ! we need many decimals

! WGS84 parameters
 a   = 6378137.000   ;   !  6378137.000   earth radius at equator
 f   = 1/298.2572236 ;   !  0.003352811   flattening
 e2  = 2*f - f*f     ;   !  0.006694380   eccentricity
 eps = e2 / (1 - e2) ;   !  0.006739497
 b   = a*(1-f)       ;   !  6356752.314   earth radius at pole

 lamb_0 = lon_ref * raddeg ;
 phi_0  = lat_ref * raddeg ;
 h_0    = alt_ref ;           value, lamb_0, phi_0, h_0;

! conversion of origin geographic coordinates into geocentric coordinates
 nu_0 = a / sqrt(1 - e2*sin(phi_0)*sin(phi_0)) ; value, nu_0, nu_0 + h_0 ;

 x0 =  (nu_0 + h_0) * cos(phi_0) * cos(lamb_0) ; value,x0;
 y0 =  (nu_0 + h_0) * cos(phi_0) * sin(lamb_0) ; value,y0;
 z0 = ((nu_0 * (1 - e2 ) + h_0)) * sin(phi_0)  ; value,z0;


! prepare new TFS table
 create, table=lon-lat-alt, column=_name, s, x, y, z, longitude, latitude, altitude, theta, phi, psi;
 nrows = table(survey, tablelength) ;


! loop over MADX elements included in "survey.tfs"
 ip = 0 ;  while (ip < nrows) { ip = ip + 1;

!    retrieve and rename MADX variables
     setvars, table=survey, row=ip;

     u = z; v = x; w = y;  value, u, v, w;

!    conversion of MAD-X coordinates into geocentric coordinates
     x_ = x0 - u*sin(lamb_0) - v*sin(phi_0)*cos(lamb_0) + w*cos(phi_0)*cos(lamb_0) ;  value, x_  ;
     y_ = y0 + u*cos(lamb_0) - v*sin(phi_0)*sin(lamb_0) + w*cos(phi_0)*sin(lamb_0) ;  value, y_  ;
     z_ = z0                 + v*cos(phi_0 )            + w*sin(phi_0)             ;  value, z_  ;

!    conversion of geocentric coordinates into geographic coordinates
     p    = sqrt(x_*x_ + y_*y_) ; q = atan((z_ * a) / (p * b));

     tan1 = z_ + eps*b*sin(q)^3 ; tan2 = p - e2 * a * cos(q)^3 ;
     phi_ = atan(tan1 / tan2) ;

     lamb = atan(y_ / x_);
     nu   = a / sqrt(1 - e2*sin(phi_)*sin(phi_)) ;
     h    = p / cos(phi_) - nu ;
     value, nu, lamb, phi_, h;

!    rename output variables
     longitude = lamb*degrad;   value, longitude;
     latitude  = phi_*degrad;   value, latitude;
     altitude  = h;             value, altitude;

!    write line for this element
     fill, table=lon-lat-alt;
 }   !! ip


! the table "lon-lat-alt" may then be accessed or written on disk
  set, format="21.15f";     ! we need many decimals

 write,  table=lon-lat-alt, file="survey.tfs";

return;
