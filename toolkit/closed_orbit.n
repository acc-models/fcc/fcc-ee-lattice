
! -------------------------------- PRINT CLOSED ORBIT OF THE RING --------------

(* Note that COD; and CODPLOT; needs to be set in the main script *)

SaveCOFile[filename_]:=Module[
{CO, rt, fn, pos},

CO = Emittance[OneTurnInformation->True, Orbit->True, Matrix->True];
rt = ClosedOrbit/.CO;

fn=OpenWrite[filename];    ! Use OpenAppend[] if you do not wish to overwrite file
$FORM="12.10";
WriteString[fn, "@ ",
                StringFill["TIME"," ", 14],
                "%s ",
                "\"",
                StringFill[DateString[]," ",-20],
                "\"",
                "\n"]; 
WriteString[fn, "@ ",
                StringFill["LENGTH"," ", 14],
                "%le",
                StringFill[ToString[LINE["LENG","$$$"]]," ",-22],
                "\n"]; 
WriteString[fn, "* ",
                StringFill["NAME"," ", 14]," ",
                StringFill["S"," ", -12],"    ",
                StringFill["X"," ", -12],"    ",
                StringFill["PX"," ", -12],"    ",
                StringFill["Y"," ", -12],"    ",
                StringFill["PY"," ", -12],"    ",
                "\n"];
WriteString[fn, "$ ",
                StringFill["%s"," ", 14]," ",
                StringFill["%le"," ", -12],"    ",
                StringFill["%le"," ", -12],"    ",
                StringFill["%le"," ", -12],"    ",
                StringFill["%le"," ", -12],"    ",
                StringFill["%le"," ", -12],"    ",
                "\n"];
pos=LINE["POSITION","*{^$$$}"]; ! Getting positions of elements 
Do[
    WriteString[fn,     " ",
                        StringFill[StringJoin["\"",LINE["NAME",pos[i]],"\""]," ", 15]," ",
                        LINE["LENG",pos[i]],"    ",
                        rt[pos[i]][[1]],"    ",
                        rt[pos[i]][[2]],"    ",
                        rt[pos[i]][[3]],"    ",
                        rt[pos[i]][[4]],"    ",
                        "\n"
                ]
    ,{i,Length[pos]}
    ];
Close[fn];
];
