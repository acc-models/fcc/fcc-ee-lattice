# FCC-ee optics repository aperture directory

This directory holds the aperture data for the FCC-ee.
So far, data is stored in a MAD-X format only.

- `FCCee_aper_definitions.madx` holds the aperture and tolerances for the main elements.
- `install_synchrotron_rad_masks.madx` install a synchrotron radiation masks close to the IP.
- `install_IP_markers.madx` install two markers left and right of the IP to cover the length of the central beam chamber.